#include "workerthread.h"

workerThread::workerThread()
{
    shouldQuit = false;
    shouldPause = false;
    workerThreadThread = nullptr;
}

workerThread::~workerThread()
{
    quit();
    while(running()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

/**
 * @brief workerThread::start Start the thread
 */
void workerThread::start()
{
    if (workerThreadThread!=nullptr) {
        throw std::exception();
    }

    workerThreadThread = new std::thread(
                &workerThread::run,this);
    workerThreadThread->detach();
}

/**
 * @brief workerThread::quit Mark the thread as quiting
 * The thread may take some time to exit after this is done, please wait until running shows false
 */
void workerThread::quit()
{
    shouldQuit = true;
    shouldPause = false;
}

/**
 * @brief workerThread::running Return if the thread is running and not going to pause next loop
 * @return If the thread is running
 */
bool workerThread::running()
{
    return (workerThreadThread!=nullptr) && !shouldPause;
}

/**
 * @brief workerThread::setPaused Set the pause state for the thread
 * @param pause If the thread should be paused when it next loops
 */
void workerThread::setPaused(bool pause)
{
    shouldPause = pause;
}

/**
 * @brief workerThread::run Begin looping this thread
 */
void workerThread::run()
{
    try {
        while(1) {
            loop();

            while(shouldPause) {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
            }

            if (shouldQuit) {
                throw std::exception();
            }
        }
    } catch (std::exception &e) {
        shouldQuit = false;
        delete workerThreadThread;
        workerThreadThread = nullptr;
    }
}
